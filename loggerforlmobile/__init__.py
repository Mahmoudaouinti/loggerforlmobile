import os
import json
import logging
import logging.config




__version__ = "0.1.0"
__author__ = 'Aouinti Mahmoud'
__credits__ = 'L-mobile solutions'


current_path = os.path.dirname(__file__)
config = current_path + "/loggerforlmobile.json"


class GetLogger:
    def __init__(self, logger_name, logging_level=logging.DEBUG, config=config):
        self.logger_name = logger_name
        self.logging_level = logging_level
        self.config = config
        self.logger = self.create()

    def create(self):
        # Create a logger
        agent_logger = logging.getLogger(self.logger_name)
        agent_logger.setLevel(self.logging_level)
        with open(self.config, 'r') as logging_configuration_file:
            config_dict = json.load(logging_configuration_file)
            logging.config.dictConfig(config_dict)
        return agent_logger