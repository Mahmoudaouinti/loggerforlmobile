from setuptools import setup



setup(
    name='loggerforlmobile',
    version='0.1.0',    
    description='Logging Python package for L-mobile solutions',
    url='https://gitlab.com/Mahmoudaouinti/loggerforlmobile.git',
    author='Aouinti Mahmoud',
    author_email='mahmoud.aouinti@l-mobile.com',
    license='MIT',
    packages=['loggerforlmobile'],
    install_requires=[ ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Development Research - L-mobile solutions 2021 Sulzbach/Murr Germany ',
        'License :: OSI Approved :: MIT',  
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 3.5',
    ],
)